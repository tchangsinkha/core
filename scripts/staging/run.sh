#!/bin/bash

# Go out to root of git folder
cd ../../

# Set test ENV
ENV="staging"
#RESULT_FOLDER="results/"$ENV

# Web
WEB="Kerry"
browserName="chrome"
language="en"
headless="false"

# Install all of required python library or other library
#pip install -r requirements.txt

# Prepare folder result
#if [ ! -d $RESULT_FOLDER ]; then
#    mkdir -p $RESULT_FOLDER
#fi
#cd $RESULT_FOLDER

# Run test robot command
rm -rf *.html *.xml *.jpg
date
/usr/local/bin/robot3 -v ENV:$ENV -v WEB:$WEB -v browserName:$browserName -v language:$language -v headless:$headless -L TRACE -d Report  -i staging --output output.xml testcases/sanity
#-i regression -e not_ready
date
/usr/local/bin/robot3 -v ENV:$ENV -v WEB:$WEB -v browserName:$browserName -v language:$language -v headless:$headless -L TRACE  --rerunfailed output.xml --output rerun.xml testcases/sanity
date
/usr/local/bin/rebot --output output.xml --merge output.xml rerun.xml
# For do not let Jenkins mark failed from shell script.
exit 0