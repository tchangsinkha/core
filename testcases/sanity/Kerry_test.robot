*** Setting ***
Resource    ${CURDIR}/../../keywords/web/page/main_page.robot
# Suite Setup    main_page.Open Web Browser And Go To EEC Website
Test Teardown    CommonWebKeywords.Test Teardown
Suite Teardown    Close All Browsers

*** Variables ***

*** Keyword ***


*** Test Cases ***
KE001 Login Success
	[Documentation]    To verify that user can login success on EasyShip
	[Tags]    natty
	main_page.Open Web Browser And Go To EasyShip    en    Chrome
	login_page.Input Username    0947586363
	login_page.Input Password    762142
	login_page.Click Recaptcha Checkbox
	login_page.Click Password Textbox
	login_page.Enter On Password Textbox
	CommonKeywords.Wait Until Page Is Completely Loaded
	main_page.Dashboard Should Be Visible