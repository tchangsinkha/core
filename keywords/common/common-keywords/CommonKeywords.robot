*** Keywords ***
Format Text
    [Arguments]    ${format_string}    &{key_value_pairs}
    ${key_value_pairs}=     Collections.Convert To Dictionary    ${key_value_pairs}
    ${result_text}    Evaluate    unicode($format_string).format(**$key_value_pairs) if sys.version_info.major==2 else str($format_string).format(**$key_value_pairs)    modules=sys
    [Return]    ${result_text}

Format Json Text
    [Documentation]    In case of json formatted text
    ...    Examples Json:
    ...    ${json1}=    { name: {value1} }
    ...    ${json2}=    { {key1}: {value1} }
    ...    We would like to format the text above with value1 = Chonnikan
    ...    Keyword calling example
    ...    | Format Json Text | ${json} | value1=Chonnikan |
    ...    | Format Json Text | ${json} | value1=Chonnikan | key1=Name
    [Arguments]    ${format_string}    &{key_value_pairs}
    ${result_text}    Evaluate    json.dumps($format_string)    json
    FOR    ${key}    IN    @{key_value_pairs.keys()}
        ${result_text}=    String.Replace String    ${result_text}    {${key}}    ${key_value_pairs}[${key}]
    END
    [Return]    ${result_text}

Get Valid File Name
    [Arguments]    ${fname}
    ${valid_fname}     Evaluate    base64.urlsafe_b64encode($fname.decode('ascii')) if sys.version_info.major==2 else base64.urlsafe_b64encode($fname.encode('UTF-8'))    modules=sys,base64
    [Return]    ${valid_fname}

Convert To Two Digit Number
    [Arguments]    ${number}
    ${number}=    Convert To String    ${number}
    ${number}=   Get Regexp Matches    ${number}    [0-9.]
    ${number}=     Catenate    @{number}
    ${number}=     Evaluate    ${number.replace(" ","")}
    ${result}=    Format String    {:.2f}    ${number}
    [Return]    ${result}

Change date time to TH format
    [Arguments]    ${date_time}    ${time_zone}=${EMPTY}    ${input_datetime_format}=DD/MM/YYYY HH.mm    ${output_datetime_format}=D MMM YYYY HH:mm
    ${fmt_datetime}=    Change date time with specific time zone and locale    ${date_time}    ${time_zone}    th    ${input_datetime_format}    ${output_datetime_format}
    [Return]    ${fmt_datetime}

Change date time to EN format
    [Arguments]    ${date_time}    ${time_zone}=${EMPTY}    ${input_datetime_format}=DD/MM/YYYY HH.mm    ${output_datetime_format}=D MMM YYYY HH:mm
    ${fmt_datetime}=    Change date time with specific time zone and locale    ${date_time}    ${time_zone}    en    ${input_datetime_format}    ${output_datetime_format}
    [Return]    ${fmt_datetime}

Change date time with specific time zone and locale
    [Arguments]    ${date_time}    ${time_zone}    ${locale}    ${input_datetime_format}=DD/MM/YYYY HH.mm    ${output_datetime_format}=D MMM YYYY HH:mm
    ${fmt_datetime}=    Run Keyword If    '${time_zone}' == '${EMPTY}'    Evaluate    arrow.get($date_time, $input_datetime_format).format($output_datetime_format, locale='${locale}')    arrow,datetime
    ...     ELSE     Evaluate    arrow.get($date_time, $input_datetime_format).to('${time_zone}').format($output_datetime_format, locale='${locale}')    arrow,datetime
    [Return]    ${fmt_datetime}

Bind Appium Driver To SeleniumLibrary
    [Documentation]    Automatically wait for sut to be in state that it can accept more actions
    ...                Timeout is amount of time to wait
    ...                Error timeout does occur either in manual or automatic mode
    ...                Automatic injection is the instrument the SUT manually with appropriate keywords 
    ...                !!! Known issue : cannot import library from robot framework, need to import SeleniumLibrary at Settings instead
    [Arguments]    ${alias}=${None}    ${testability}=${False}    ${automatic_wait}=${False}    ${timeout}=30    ${error_timeout}=${False}    ${automatic_injection}=${True}
    SeleniumTestability.Bind Appium Driver To Selenium    ${alias}

Connect To Database With SSH
    [Documentation]    Import Library ConnectWithSSH.py
    ...                This keyword use for connect Mysql Database wih SSH Tunnel
    ...                ${ssh_pkfile} variable is a SSH private key that send by file directory path e.g./user/foo/ssh_user
    ...                ${ssh_host} variable is a SSH host
    ...                ${ssh_user} variable is a SSH username
    ...                ${ssh_port} variable is a SSH port
    ...                ${dbName} variable is a Database name
    ...                ${dbUsername} variable is a Database username
    ...                ${dbPassword} variable is a Database password
    ...                ${dbHost} variable is a Database host
    ...                ${dbPort} variable is a Database port
    ...                ${dbapiModuleName} variable is a Database connection modules e.g. Mysql send by pymysql, PostgreSQL send by psycopg2
    [Arguments]    ${ssh_pkfile}    ${ssh_host}    ${ssh_user}    ${ssh_port}    ${dbName}    ${dbUsername}    ${dbPassword}    ${dbHost}    ${dbPort}    ${dbapiModuleName}=pymysql
    ConnectWithSSH.Connect To Mysql Database Over SSH    ssh_pkfile=${ssh_pkfile}
    ...    ssh_host=${ssh_host}
    ...    ssh_user=${ssh_user}
    ...    ssh_port=${ssh_port}
    ...    dbapiModuleName=${dbapiModuleName}
    ...    dbName=${dbName}
    ...    dbUsername=${dbUsername}
    ...    dbPassword=${dbPassword}
    ...    dbHost=${dbHost}
    ...    dbPort=${dbPort}

Close Database Connection
    [Documentation]    Import Library ConnectWithSSH.py
    ...                This keyword use for close Database and SSH connection, kindly use after finished your database connection.
    ConnectWithSSH.Close SSH

Encode Basic Authentication
    [Arguments]    ${username}    ${password}
    ${text} =    Catenate    SEPARATOR=    ${username}    :    ${password}
    ${authorise_key} =    Encode text to base64    ${text}
    ${basic_auth_key} =    Catenate    Basic    ${authorise_key}
    [Return]    ${basic_auth_key}

Encode text to base64
    [Arguments]    ${text}
    ${base64_text} =    Evaluate    base64.b64encode(b'${text}')     modules=base64
    [Return]    ${base64_text}

Convert price to total amount format
    [Arguments]     ${price}
    ${price}=    Convert To Number    ${price}
    ${price}=     Evaluate     '{:,.2f}'.format($price)
    [Return]     ${price}

Convert price to total amount int format
    [Arguments]     ${price}
    ${price}=    Convert To Integer    ${price}
    ${price}=     Evaluate     '{:,}'.format($price)
    [Return]     ${price}

Convert Price To Us Format
    [Arguments]    ${price}
    ${str_price}=    Convert To String    ${price}
    ${split_price}=    Split String    ${str_price}    .
    ${length}=    Get Length    ${split_price}
    ${deci}=    Run Keyword If    ${length}>1    Evaluate    ${split_price}[1]
    ${result}=    Run Keyword If    ${length}==1 or ${deci}==0    Convert price to total amount int format    ${price}
    ...    ELSE    Convert price to total amount format    ${price}
    [Return]    ${result}

Switch context to the latest
    ${webview} =    Get Contexts
    ${length} =    Get Length    ${webview}
    Switch To Context    ${webview}[${length-1}]