###################################################################################################
# How to use SeleniumRetryRunKeyword:
# Go to Robotframework import Library with parameter attributes
#
# e.g.1 : Import Library with only plugin.
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumRetryRunKeyword.py
#
# e.g.2 : SeleniumLibrary will do retry follow by total round and wait follow by interval time. Default values retries as 3 round and interval time in 10 sec.
#         Go to import Library with retry and interval time in sec that send by key [retries, interval_time_in_sec] in integer style.
#         - retries=2
#         - interval_time_in_sec=30
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumRetryRunKeyword.py;retries=2;interval_time_in_sec=30
#
# e.g.3 : SeleniumLibrary will retry run keyword that match in kw_inclusion_list and not retry for kw_inclusion_list.
#         Go to import Library with keyword inclusion/ exclusion that send by key [kw_inclusion_list, kw_exclusion_list] in regular expression style. If multiple values accepted with # as a delimiter.
#         - kw_inclusion_list=click.*#get.* :: SeleniumLibrary will retry on keyword that start with click and get e.g. Click Element, Click Button, Get WebElement etc.
#           **Default values as click.* input.*
#         - kw_exclusion_list=open.* :: SeleniumLibrary will not retry on keyword that start with open e.g. Open Browser etc.
#           **Default value as None
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumRetryRunKeyword.py;kw_inclusion_list=click.*#get.*;kw_exclusion_list=open.*
#
# e.g.4 : Import Library with all values
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumRetryRunKeyword.py;2;30;kw_inclusion_list=click.*#get.*;kw_exclusion_list=open.*
###################################################################################################


from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn

from SeleniumLibrary.base import LibraryComponent, keyword, DynamicCore
from SeleniumLibrary.locators import ElementFinder
from SeleniumLibrary import SeleniumLibrary

from time import sleep
import types
import re

retries = None
interval_time_in_sec = None


class SeleniumRetryRunKeyword(LibraryComponent):

    def __init__(
            self: 'SeleniumRetryRunKeyword',
            ctx: SeleniumLibrary,
            retries: int = 3,
            interval_time_in_sec: int = 10,
            **init_kwargs):

        LibraryComponent.__init__(self, ctx)
        self.ctx = ctx
        ctx._original_run_keyword = ctx.run_keyword
        ctx.retries = int(retries)+1
        ctx.interval_time_in_sec = int(interval_time_in_sec)
        ctx.kw_inclusion_list = init_kwargs["kw_inclusion_list"].split(
            '#') if 'kw_inclusion_list' in init_kwargs else 'click.*#input.*'.split('#')
        ctx.kw_exclusion_list = init_kwargs["kw_exclusion_list"].split(
            '#') if 'kw_exclusion_list' in init_kwargs else []
        self._monkey_patch(ctx)

    def _monkey_patch(self, ctx):
        def run_keyword(self, name, args, kwargs):
            inclusion_match = list(filter(lambda match_regex: match_regex if re.search(
                match_regex, name, re.IGNORECASE) else None, self.kw_inclusion_list))
            exclusion_match = list(filter(lambda match_regex: match_regex if re.search(
                match_regex, name, re.IGNORECASE) else None, self.kw_exclusion_list))
            if inclusion_match and not exclusion_match:
                for i in range(self.retries):
                    try:
                        if i > 0:
                            logger.info('Starting retry round: {}'.format(i))
                        return self._original_run_keyword(name, args, kwargs)
                    except Exception as e:
                        if i < self.retries-1:
                            sleep(self.interval_time_in_sec)
                            continue
                        else:
                            ctx.failure_occurred()
                            logger.info("{}: {}".format(type(e).__name__, e))
                            raise e
                    break
            else:
                return self._original_run_keyword(name, args, kwargs)
        ctx.run_keyword = types.MethodType(run_keyword, ctx)
