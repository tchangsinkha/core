###################################################################################################
# How to use SeleniumCaptureScreen:
# Go to Robotframework import Library with parameter attributes
#
# e.g.1 : Import Library with only plugin.
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumCaptureScreen.py
#
# e.g.2 : SeleniumLibrary will retry run keyword that match in kw_inclusion_list and not retry for kw_inclusion_list.
#         Go to import Library with keyword inclusion/ exclusion that send by key [kw_inclusion_list, kw_exclusion_list] in regular expression style. If multiple values accepted with # as a delimiter.
#         - kw_inclusion_list=click.*#get.* :: SeleniumLibrary will retry on keyword that start with click and get e.g. Click Element, Click Button, Get WebElement etc.
#           **Default values as click.* input.*
#         - kw_exclusion_list=open.* :: SeleniumLibrary will not retry on keyword that start with open e.g. Open Browser etc.
#           **Default values as None
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumCaptureScreen.py;kw_inclusion_list=click.*#get.*;kw_exclusion_list=open.*
#
# e.g.3 : Import Library with all values
# Library    SeleniumLibrary    run_on_failure=None    plugins=${CURDIR}/SeleniumCaptureScreen.py;kw_inclusion_list=click.*#get.*;kw_exclusion_list=open.*
###################################################################################################


from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn

from SeleniumLibrary.base import LibraryComponent, keyword, DynamicCore
from SeleniumLibrary.locators import ElementFinder
from SeleniumLibrary import SeleniumLibrary
import unicodedata
from time import sleep
from time import time
import types
import re


class SeleniumCaptureScreen(LibraryComponent):

    def __init__(
            self: 'SeleniumCaptureScreen',
            ctx: SeleniumLibrary,
            **init_kwargs):

        LibraryComponent.__init__(self, ctx)
        self.ctx = ctx
        ctx._original_run_keyword_screen = ctx.run_keyword
        ctx.kw_inclusion_list = init_kwargs["kw_inclusion_list"].split(
            '#') if 'kw_inclusion_list' in init_kwargs else 'click.*#input.*'.split('#')
        ctx.kw_exclusion_list = init_kwargs["kw_exclusion_list"].split(
            '#') if 'kw_exclusion_list' in init_kwargs else []
        self._monkey_patch(ctx)

    def _monkey_patch(self, ctx):
        def run_keyword(self, name, args, kwargs):
            inclusion_match = list(filter(lambda match_regex: match_regex if re.search(
                match_regex, name, re.IGNORECASE) else None, self.kw_inclusion_list))
            exclusion_match = list(filter(lambda match_regex: match_regex if re.search(
                match_regex, name, re.IGNORECASE) else None, self.kw_exclusion_list))
            if inclusion_match and not exclusion_match:
                try:
                    logger.info("Take screenshot before step: {}".format(name))
                    test_name = BuiltIn().get_variable_value('${TEST NAME}') 
                    test_name = test_name if test_name else ""
                    suite_name = BuiltIn().get_variable_value('${SUITE NAME}')
                    import base64
                    fname = base64.urlsafe_b64encode(
                        (suite_name+test_name+str(time())+".png").encode()).decode()
                    self.capture_page_screenshot(fname+".png")
                except Exception as e:
                    logger.warn("Cannot take the screenshot for this step {}: {}".format(
                        type(e).__name__, e))
            return self._original_run_keyword_screen(name, args, kwargs)
        ctx.run_keyword = types.MethodType(run_keyword, ctx)
