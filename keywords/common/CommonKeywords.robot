*** Settings ***
Resource    ${CURDIR}/../../resources/imports.robot

*** Variables ***
${GLOBALTIMEOUT}        ${15}
${GLOBALWAITTIME}       ${30}
${speed} 	${0.1}


*** Keywords ***
Wait Until Page Is Completely Loaded
	[Arguments]     ${time}=${GLOBALWAITTIME}
	sleep    ${time} sec