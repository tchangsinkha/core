*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/page/main_page.robot

*** Variables ***

*** Keywords ***
Input Username
    [Arguments]    ${username}
    SeleniumLibrary.Input Text    ${dictLoginPage}[txt_username]    ${username}

Input Password
    [Arguments]    ${password}
    SeleniumLibrary.Input Password    ${dictLoginPage}[txt_password]    ${password}

Click Recaptcha Checkbox
    SeleniumLibrary.Click Element    ${dictLoginPage}[chk_recaptcha]

Click Login Button
    SeleniumLibrary.Element Should Be Visible    ${dictLoginPage}[btn_login]
    SeleniumLibrary.Click Element    ${dictLoginPage}[btn_login]

Click Password Textbox
    SeleniumLibrary.Click Element    ${dictLoginPage}[txt_password]

Enter On Password Textbox
    SeleniumLibrary.Press Keys    ${dictLoginPage}[txt_password]    RETURN