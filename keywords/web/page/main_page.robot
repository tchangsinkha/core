*** Setting ***
Resource    ${CURDIR}/../../../resources/imports.robot
Variables   ${CURDIR}/../../../resources/${WEB}/translation_${language}.yaml
Resource    ${CURDIR}/../../../keywords/common/CommonKeywords.robot
Resource    ${CURDIR}/../../../keywords/common/common-keywords/CommonWebKeywords.robot
Resource    ${CURDIR}/../../../keywords/common/common-keywords/CommonKeywords.robot
Variables   ${CURDIR}/../../../resources/${WEB}/testdata/${ENV}/test_data.yaml
Resource    ${CURDIR}/../../../resources/${WEB}/${WEB}_Locators.robot
Resource    ${CURDIR}/../../../keywords/web/page/contact_us_page.robot

Resource    ${CURDIR}/../../../keywords/web/page/billing_address_page.robot
Resource    ${CURDIR}/../../../keywords/web/page/change_password_page.robot
Resource    ${CURDIR}/../../../keywords/web/page/forgot_password_page.robot
Resource    ${CURDIR}/../../../keywords/web/page/login_page.robot
Resource    ${CURDIR}/../../../keywords/web/page/register_page.robot

*** Variables ***

*** Keywords ***
Open Web Browser And Go To EasyShip
    [Arguments]    ${language}    ${browser}
    Run Keyword If    '${language}' == 'th'    SeleniumLibrary.Open Browser    ${customer_host}    ${browser}
    ...    ELSE IF    '${language}' == 'en'    SeleniumLibrary.Open Browser    ${customer_host_en}    ${browser}
    Maximize Browser Window

Dashboard Should Be Visible
    SeleniumLibrary.Element Should Be Visible    ${dictMainPage}[lbl_dashboard]