*** Variables ***
#####LOGIN PAGE#####
# &{dictContactPage}
# ...    contact_us=xpath=//*[@class='site-footer']//*[contains(., '${txt_contact_us}')]
# ...    address_map=css=#mapPopUp
# ...    ecc_name=xpath=//*[@class='section-hero-header-title']//*[contains(., '${ecc_name}')]
# ...    txt_username=css=#username
# ...    txt_password=css=#password
# ...    btn_login=css=.radius
# ...    msg_loginpage=css=#flash
# ...    btn_logout=xpath=//a[@class='button secondary radius']//i[@class='icon-2x icon-signout' and contains(.,'${txt_logout}')]
&{dictLoginPage}
...    txt_username=css=#inputMobileNo
...    txt_password=css=#inputPassword
...    chk_recaptcha=css=iframe[title='reCAPTCHA']
...    btn_login=css=button[type='submit']

&{dictMainPage}
...    lbl_dashboard=xpath=//div[@class='container-fluid']